+++
title = "Hi. I'm Mark"
date = "2021-01-04"
aliases = ["about-us","about-mark","contact"]
[ author ]
  name = "Mark Florian"
+++

> Though an astrophysicist by training, Mark decided to pursue a career where he had a bit more agency, and which measured timescales in days rather than billions of years. Frontend development seemed a perfect fit, where Mark enjoys writing correct, pragmatic and simple user interfaces. He loves to ski, travel, eat good food, and generally contemplate just how good dogs are.
