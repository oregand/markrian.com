# Merry Christmas Mark!

![Happy Xmas Mark!](https://media.giphy.com/media/3oz8xALpV1X2BPo7cI/giphy.gif)

## General information

This is a small website that I thought you might like as a starter for your own personal site written in Hugo(something Paul got me into). If this is not your style or you would rather not use this there is no hard feelings at all, I just wanted to try and give you something that might be useful and fun! 

## Get Started

- Project init with sub-module

```bash
$ git clone git@gitlab.com:oregand/markrian.com.git
$ git submodule init 
$ git submodule update
```

- Development server

```bash
$ brew install hugo
$ hugo version
$ hugo server -D
```

- Static pages

```bash
$ hugo -D
```

## Hosting

Hugo can be hosted in lots of [spaces](https://gohugo.io/hosting-and-deployment/) but I think using it with [GitLab Pages](https://gohugo.io/hosting-and-deployment/hosting-on-gitlab/) would be super neat! 

## Theme

This site uses the [hugo-theme-hello-friend-ng](https://github.com/rhazdon/hugo-theme-hello-friend-ng.git) under the hood for styles but all the general [Hugo commands](https://gohugo.io/content-management/) work fine! 

## What now?

Stay safe and hack away is my suggestion :)